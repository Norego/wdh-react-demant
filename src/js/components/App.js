import React, { Component } from 'react';
import { debounce } from 'lodash';
import Search from './Search';
import LocationListContainer from './LocationListContainer';
import data from '../../data.json';
import { LocationDetails } from "./LocationDetails";

class App extends Component {
  constructor() {
    super();

    this.state = {
      data: data.Clinics,
      searching: false,
      searchResults: [],
      selectedLocation: null,
    };

    this.onSearch = debounce(this.onSearch.bind(this), 1000);
  }

  updateState(state, cb) {
    this.setState(state, cb);
  }

  onSearch(searchInput) {
    if (searchInput.length > 0) {
      this.updateState({searching: true});
    } else {
      this.updateState({searching: false});
    }

    let regExp = new RegExp(searchInput, 'i');
    let results = [];

    this.state.data.forEach(location => {
      Object.values(location).some(value => {

        if (value.toString().match(regExp)) {
          results.push(location);

          return true;
        }
      })
    });

    this.updateState({searchResults: results});
  }

  selectLocation(self, location) {
    self.updateState({
      selectedLocation: location,
    })
  }

  render() {
    return (
      <div id="app">
        <h1>WDH React</h1>

        {!this.state.selectedLocation &&
        (
          <>
            <Search onSearch={this.onSearch}
            />
            <LocationListContainer data={this.state.data}
                                   searching={this.state.searching}
                                   searchResults={this.state.searchResults}
                                   selectLocation={(location) => this.selectLocation(this, location)}
            />
          </>
        )
        }

        {this.state.selectedLocation &&
        (
          <>
            <LocationDetails location={this.state.selectedLocation}/>
            <button onClick={() => this.updateState({selectedLocation: null})}>
              Back
            </button>
          </>
        )
        }
      </div>
    );
  }
}

export default App;
