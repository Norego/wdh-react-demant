import React, { Component } from "react";

export class LocationDetails extends Component {
  constructor(props) {
    super(props);
  }

  getReadableName(key) {
    return locationRowNames[key] || key;
  }

  buildTableRow(name, value) {
    return (
      <tr key={this.props.location.Name + name}>
        <td>
          {name}
        </td>
        <td>
          {value}
        </td>
      </tr>
    )
  }

  render() {
    const location = this.props.location;

    if (!location) return null;

    const tableData = Object.entries(location).filter((value) => value[0].toLowerCase() !== 'name')


    return (
      <div className="location-details">
        <span>
          {location.Name}
        </span>
        <table>
          <tbody>
          {
            tableData.map(entry => this.buildTableRow(this.getReadableName(entry[0]), entry[1]))
          }
          </tbody>
        </table>
      </div>
    );
  }
}

const locationRowNames = {
  "DistanceToPoint": "Distance to point",
  "FormattedAddress": "Address",
}
